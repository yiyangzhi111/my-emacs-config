(defun my-org-hook ()
  (outline-hide-sublevels 1))
(add-to-list 'file-coding-system-alist'("\\.org\\'" . utf-8))
(use-package org
  :init
  (require 'ox-gfm nil t)
  :hook (org-mode . my-org-hook)
  :config
  ;;(set-default-font "Source Code Pro")
  ;; 在source block内使用tab
  (setq org-confirm-babel-evaluate nil
        org-src-fontify-natively t
        org-src-tab-acts-natively t)
  (setq org-capture-template
        '(("t" "Todo" entry (file+headline "~/org/gtd.org" "Tasks")
           "* TODO %?\n  %i\n  %a")
          ("j" "Journal" entry (file+datetree "~/org/journal.org")
           "* %?\nEntered on %U\n  %i\n  %a")))
  (setq org-latex-pdf-process '("xelatex -interaction nonstopmode %f" "xelatex -interaction nonstopmode %f"))

  (org-babel-do-load-languages
   'org-babel-load-languages
   '(
     (shell         . t)
     (js         . t)
     (emacs-lisp . t)
     ;; (perl       . t)
     ;;   (scala      . t)
     (clojure    . t)
     (python     . t)
     ;; (ruby       . t)
     (dot        . t)
     (css        . t)
     (plantuml   . t)))
  (setq org-babel-python-command "python3")
  ;; 设置以便让org-mode知道再哪些文件中搜寻TODO和计划条目
  (setq org-agenda-files (list "~/org/todo.org"
                               "~/org/work.org"))

  (setq org-todo-keywords
        '((sequence "TODO" "DOING" "|" "DONE" "ABORT")))
  :hook
  (org-mode . (lambda()(linum-mode 0)))
  (org-after-todo-statistics . (lambda (n-done n-not-done)
    "Switch entry to DONE when all subentries are done, to TODO otherwise."
    (let (org-log-done org-log-states)   ; turn off logging
      (org-todo (if (= n-not-done 0) "DONE" "TODO")))))
  ;; (add-hook 'org-mode-hook
  ;;           (lambda () (face-remap-add-relative 'default :family "Monospace")))
  :bind
  ("\C-cl" . org-store-link)
  ("\C-cc" . org-capture)
  ("\C-ca" . 'org-agenda)
  ("\C-ct" . 'org-tags-view)
  ;; (global-set-key "\C-cb" 'org-iswitchb)
  )
(provide 'init-org)
