(use-package image+
  :init
  :hook
  (image-mode . (lambda() (imagex-auto-adjust-mode 1)))
 )
(provide 'init-image)
