;;;; CC-mode配置  http://www.gnu.org/software/emacs/manual/html_mono/ccmode.html
(add-to-list 'auto-mode-alist '("\\.cpp\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.hxx\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.hpp\\'" . c++-mode))
(add-to-list 'auto-mode-alist '("\\.cxx\\'" . markdown-mode))

(use-package cc-mode
  :init
  )
(provide 'init-cc)
