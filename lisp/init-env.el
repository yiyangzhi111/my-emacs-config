(when (eq system-type 'darwin)
  (require 'exec-path-from-shell)
  (exec-path-from-shell-initialize)
  (setq exec-path-from-shell-arguments "")
  (setq mac-option-modifier 'alt)
  (setq mac-command-modifier 'meta)
  )

(when (eq system-type 'windows-nt)
	(setenv "MSYS2_PATH_TYPE" "inherit")
	(setenv "MSYSTEM" "MINGW64")
	(setenv "TERM" "xterm")
	(setenv "PS1" "\\e[0;32m\\u@\\h \\e[35m\\]$MSYSTEM\\[\\e[0m\\] \\[\\e[33m\\]\\w\\[\\e[0m\\]\\n\\$")
	(setq shell-file-name "C:/msys64/usr/bin/bash.exe")
	;; (setq  explicit-bash-args '("--login" "-i"))
	(autoload 'ansi-color-for-comint-mode-on "ansi-color" nil t)
	(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
	(add-hook 'shell-mode-hook 'mywinshell)
	(add-to-list 'process-coding-system-alist
		     '("bash" . (undecided-dos . undecided-unix)))
	(global-set-key (kbd "C-c z") 'shell)
	(global-set-key (kbd "<f10>") 'rename-buffer)
)
(provide 'init-env)
