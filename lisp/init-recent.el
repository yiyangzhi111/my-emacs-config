﻿;; copy from http://blog.chinaunix.net/uid-7717190-id-2564927.html
(defun my-recentf-open ()
  "open recent files.  In ido style if applicable --lgfang"
  (interactive)
  (let* ((path-table (mapcar
                     (lambda (x) (cons (file-name-nondirectory x) x))
                      recentf-list))
         (file-list (mapcar (lambda (x) (file-name-nondirectory x))
                            recentf-list))
         (complete-fun (if (require 'ido nil t)
                           'ido-completing-read
                         'completing-read))
         (fname (funcall complete-fun "File Name: " file-list)))
        (find-file (cdr (assoc fname path-table)))))
;;; recently opened file
(use-package recentf
  :init
  (recentf-mode 1)
  ;; add at the front of list, don't conncect to remote hosts
  (add-to-list 'recentf-keep 'file-remote-p)
  :config
  (setq recentf-max-saved-items 100)
  (setq ido-enable-flex-matching t)
  :bind ([f5] . my-recentf-open)
  )
(provide 'init-recent)
