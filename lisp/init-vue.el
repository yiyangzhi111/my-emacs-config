(use-package vue-mode
  :init
  (setq mmm-js-mode-enter-hook (lambda () (setq syntax-ppss-table nil)))
  (setq mmm-typescript-mode-enter-hook (lambda () (setq syntax-ppss-table nil)))
  (setq indent-tabs-mode nil
        js-indent-level 2)
  (setq css-indent-offset 2)
)
(provide 'init-vue)
