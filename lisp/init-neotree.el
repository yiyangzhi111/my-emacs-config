(use-package neotree
  :init
  (setq projectile-switch-project-action 'neotree-projectile-action)
  :bind(([f8] . neotree-projectile-action))
)
