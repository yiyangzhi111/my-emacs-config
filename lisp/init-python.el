;; sudo apt install elpa-elpy
;; sudo apt install python3-jedi black python3-autopep8 yapf3 python3-yapf
(use-package elpy
  :defer t
  :init
  (advice-add 'python-mode :before 'elpy-enable)
  (setq elpy-rpc-ignored-buffer-size 1024000)
  (setq elpy-rpc-python-command "python3")
  ;; (pyvenv-activate "/Users/super/venv")
  :hook
  (python-mode . elpy-enable)
  ;; (add-hook 'python-mode-hook 'pylint-add-menu-items)
  ;; (add-hook 'python-mode-hook 'pylint-add-key-bindings)
  )
;; (add-hook 'python-mode-hook
;;           (lambda()
;;             (define-key python-mode-map (kbd "C-c C-j") nil)))
(setq elpy-mode-map
      (let ((m (make-sparse-keymap)))
        ;; (define-key m (kbd "C-c C-a") #'go-import-add)
        (define-key m (kbd "C-c C-j") #'elpy-goto-definition)
        ;; go back to point after called godef-jump.  ::super
        (define-key m (kbd "C-c C-b") #'pop-tag-mark)
        ;; (define-key m (kbd "C-x 4 C-c C-j") #'godef-jump-other-window)
        ;; (define-key m (kbd "C-c C-d") #'go-guru-describe)
        m))
(add-to-list 'process-coding-system-alist '("python" . (utf-8 . utf-8)))

(use-package auto-virtualenv
  :ensure t
  :init
  (use-package pyvenv
    :ensure t)
  :config
  (add-hook 'python-mode-hook 'auto-virtualenv-set-virtualenv)
  (add-hook 'projectile-after-switch-project-hook 'auto-virtualenv-set-virtualenv)  ;; If using projectile
  )

(provide 'init-python)
