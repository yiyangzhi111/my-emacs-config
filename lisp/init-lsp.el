(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c s")
  :config
  (setq lsp-auto-guess-root t)
  (define-key lsp-mode-map (kbd "C-c s") lsp-command-map)
;;  (setq lsp-go-directory-filters )
  :bind
  (:map lsp-mode-map
        ("C-c C-j" . lsp-find-definition)
        ("C-c C-l" . lsp-find-references)
        ("C-c C-r" . lsp-rename)
        ("C-c C-b" . pop-tag-mark)
        )
  :hook (go-mode . lsp)
  :hook (vue-mode . lsp)
  :hook (c-mode . lsp)
  :hook (c++-mode . lsp)
  :hook (rust-mode . lsp)
  :hook (dart-mode . lsp)
;;  :hook (python-mode . lsp)
  )

;; Set up before-save hooks to format buffer and add/delete imports.
;; Make sure you don't have other gofmt/goimports hooks enabled.
(defun lsp-go-install-save-hooks ()
  (setq lsp-diagnostic-package :none)
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
(add-hook 'go-mode-hook #'lsp-go-install-save-hooks)


;; Optional - provides fancier overlays.
(use-package lsp-ui
  :commands lsp-ui-mode)

;; Company mode is a standard completion package that works well with lsp-mode.
(use-package company
  :config
  ;; Optionally enable completion-as-you-type behavior.
  (setq company-idle-delay 0)
  (setq company-minimum-prefix-length 1))

;; Optional - provides snippet support.
(use-package yasnippet
  :commands yas-minor-mode
  :hook (go-mode . yas-minor-mode))

(use-package helm-lsp :commands helm-lsp-workspace-symbol)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

;; optionally if you want to use debugger
;; (use-package dap-mode)
;; (use-package dap-LANGUAGE) to load the dap adapter for your language

(provide 'init-lsp)
