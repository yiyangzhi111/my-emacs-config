(use-package yasnippet
  :commands yas-minor-mode
  :init
  (yas-global-mode 1)
  (add-to-list 'yas-snippet-dirs (concat user-emacs-directory "/plugin/snippets") t)
  (defface ac-yasnippet-candidate-face
    '((t (:background "sandybrown" :foreground "black")))
    "Face for yasnippet candidate.")

  (defface ac-yasnippet-selection-face
    '((t (:background "coral3" :foreground "white")))
    "Face for the yasnippet selected candidate.")

  (defvar ac-source-yasnippet
    '((candidates . ac-yasnippet-candidate)
      (action . yas/expand)
      (candidate-face . ac-yasnippet-candidate-face)
      (selection-face . ac-yasnippet-selection-face))
    "Source for Yasnippet.")
  :hook (go-mode . yas-minor-mode)
)
(provide 'init-yasnippet)
