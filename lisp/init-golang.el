(use-package go-mode
  :init
  ;; Set up before-save hooks to format buffer and add/delete imports.
  ;; Make sure you don't have other gofmt/goimports hooks enabled.
  ;; define my own go mode key binds ::super
  (setq go-mode-map
        (let ((m (make-sparse-keymap)))
          (define-key m "}" #'go-mode-insert-and-indent)
          (define-key m ")" #'go-mode-insert-and-indent)
          (define-key m "," #'go-mode-insert-and-indent)
          (define-key m ":" #'go-mode-insert-and-indent)
          (define-key m "=" #'go-mode-insert-and-indent)
          ;; (define-key m (kbd "C-c C-a") #'go-import-add)
          ;; (define-key m (kbd "C-x 4 C-c C-j") #'godef-jump-other-window)
          ;; (define-key m (kbd "C-c C-d") #'go-guru-describe)
          m))
  ;; use goimports instead of gofmt ::super
  ;;(setq temporary-file-directory "/dev/shm")

  :hook
  (go-mode . (lambda ()
		     (setq-default)
		     (setq tab-width 4)
		     (setq standard-indent 2)
		     (setq indent-tabs-mode nil)
		     (lsp-go-install-save-hooks)))
  ;; (setq go-test-args "-v")
  )
(provide 'init-golang)
