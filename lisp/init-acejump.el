(use-package ace-jump-mode
;;
;; ace jump mode major function
  ;;
  :init
  :config
  (ace-jump-mode-enable-mark-sync)

;; you can select the key you prefer to
  :bind (("C-j" . ace-jump-mode)
         ("C-x p" . ace-jump-mode-pop-mark)
  )
)
(provide 'init-acejump)
