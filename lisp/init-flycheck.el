(use-package flycheck
  :init
  :config (global-flycheck-mode)
  :hook (go-mode . (lambda()
		     (message "flycheck hooked")
                     (setq flycheck-checker 'go-staticcheck)
                     ;; (setq flycheck-go-build-tags "-ST1001")
;;                     (setq flycheck-go-version "staticcheck -ST1001")
		     ))
  :bind
  ("\C-cl" . flycheck-list-errors)
  ("\C-cp" . flycheck-previous-error)
  ("\C-cn" . flycheck-next-error)
  )

(provide 'init-flycheck)
