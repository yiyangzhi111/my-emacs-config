;; https://github.com/emacs-jp/replace-colorthemes
(use-package color-theme-modern
  :init
  ;; (load-theme 'calm-forest t)
  ;; (load-theme 'arjen t)
  ;; (load-theme 'billw t)
  ;; (load-theme 'clarity t)
  ;; (load-theme 'hober t)
  ;; (load-theme 'blue-gnus t)
  ;; (load-theme 'jsc-light2 t)
  ;; (load-theme 'euphoria t)
  (setq my-color-themes (list 'aalto-dark
                              'aalto-light
                              'aliceblue
                              'andreas
                              'arjen
                              'beige-diff
                              'beige-eshell
                              'bharadwaj-slate
                              'bharadwaj
                              'billw
                              'black-on-gray
                              'blippblopp
                              'blue-erc
                              ;; 'blue-eshell
                              'blue-gnus
                              'blue-mood
                              'blue-sea
                              'calm-forest
                              'charcoal-black
                              'clarity
                              'classic
                              'cobalt
                              'comidia
                              'dark-blue
                              'dark-blue2
                              'dark-erc
                              'dark-font-lock
                              'dark-gnus
                              'dark-green
                              'dark-info
                              'dark-laptop
                              'deep-blue
                              'desert
                              'digital-ofs1
                              'emacs-21
                              'emacs-nw
                              'euphoria
                              'feng-shui
                              'fischmeister
                              'gnome
                              'gnome2
                              'goldenrod
                              'gray1
                              'gray30
                              'greiner
                              'gtk-ide
                              'high-contrast
                              'hober
                              'infodoc
                              'jb-simple
                              'jedit-grey
                              'jonadabian-slate
                              'jonadabian
                              'jsc-dark
                              'jsc-light
                              'jsc-light2
                              'katester
                              'kingsajz
                              'late-night
                              'lawrence
                              'ld-dark
                              'lethe
                              'marine
                              'marquardt
                              'matrix
                              'midnight
                              'mistyday
                              'montz
                              'oswald
                              'parus
                              'pierson
                              'pok-wob
                              'pok-wog
                              'ramangalahy
                              'raspopovic
                              'renegade
                              'resolve
                              'retro-green
                              'retro-orange
                              'robin-hood
                              'rotor
                              'ryerson
                              'salmon-diff
                              'salmon-font-lock
                              'scintilla
                              'shaman
                              'simple-1
                              'sitaramv-nt
                              'sitaramv-solaris
                              'snow
                              'snowish
                              'standard-ediff
                              'standard
                              'subtle-blue
                              'subtle-hacker
                              'taming-mr-arneson
                              'taylor
                              'tty-dark
                              'vim-colors
                              'whateveryouwant
                              'wheat
                              'word-perfect
                              'xemacs
                              'xp))
  (defun my-theme-set-default () ; Set the first row
    (interactive)
    (setq theme-current my-color-themes)
    (load-theme (car theme-current) t t)
    (enable-theme (car theme-current)))

  (defun my-describe-theme () ; Show the current theme
    (interactive)
    (message "%s" (car theme-current)))

  (setq using-theme nil)
   ; Set the next theme (fixed by Chris Webber - tanks)
  (defun my-theme-cycle ()
    (interactive)
    (setq theme-current (cdr theme-current))
    (if (null theme-current)
        (setq theme-current my-color-themes))
    (if (not (null using-theme))
        (disable-theme using-theme))
    (setq using-theme (car theme-current))
    (load-theme (car theme-current) t t)
    (enable-theme (car theme-current))
    (message "%S" (car theme-current)))

  (setq theme-current my-color-themes)
  (setq color-theme-is-global nil) ; Initialization
  ;;(my-theme-set-default)
  (global-set-key [f12] 'my-theme-cycle)
)
(provide 'init-theme)

