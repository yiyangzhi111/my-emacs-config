(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  ;; :init (setq markdown-command "multimarkdown"))

  :init (setq markdown-command "blackfriday-tool"))
;; (add-to-list 'auto-mode-alist '("\\.markdown\\'" . gfm-mode))
;; (add-to-list 'auto-mode-alist '("\\.md\\'" . gfm-mode))
;; http://github.com/russross/blackfriday-tool/
;; (setq markdown-command "blackfriday-tool")
(provide 'init-md)
