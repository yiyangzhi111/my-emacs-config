;; 关闭工具栏
(tool-bar-mode 0)
(menu-bar-mode 0)
(scroll-bar-mode 0)
(add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
(add-to-list 'default-frame-alist '(ns-appearance . dark))

;; 外部复制
(setq x-select-enable-clipboard t);

;;关闭烦人的出错时的提示声
(setq visible-bell t)

;;关闭起动时的那个“开机画面”。
(setq inhibit-startup-message t)
;; *scratch* buffer的提示信息
(setq initial-scratch-message "---------------------------------------------------------------------------
                            天地玄宗，万炁本根。
                            广修亿劫，证吾神通。
                            三界内外，惟道独尊。
                            体有金光，覆映吾身。
                            视之不见，听之不闻。
                            包罗天地，养育群生。
                            受持万遍，身有光明。
                            三界侍卫，五帝司迎。
                            万神朝礼，驭使雷霆。
                            鬼妖丧胆，精怪忘形。
                            内有霹雳，雷神隐名。
                            洞慧交彻，五炁腾腾。
                            金光速现，覆护真人（吾身）。
----------------------------------------------------------------------------")
;;显示日期
;; (setq display-time-day-and-date t)
;;显示时间
;; (display-time)
;;时间为24小时制
;; (setq display-time-24hr-format t)
;;时间显示包括日期和具体时间
;; (setq display-time-day-and-date t)
;;时间栏旁边启动邮件设置
;; (setq display-time-use-mail-icon t)
;;时间的变化频率
;; (setq display-time-interval 10)

;; 我的信息
(setq user-full-name "Super")
(setq user-mail-address "yiyangzhi111@gmail.com")

;;不要在鼠标点击的那个地方插入剪贴板内容
(setq mouse-yank-at-point t)
;; 设置光标
;;(setq-default cursor-type 'bar)
;;很大的 kill ring
(setq kill-ring-max 200)

;;把 fill-column 设为 120
(setq default-fill-column 120)


;;在标题栏显示buffer的名字
(setq frame-title-format "%b")

;;设置默认得major mode
(setq major-mode 'text-mode)

;;括号匹配时显示另外一边的括号，而不是烦人的跳到另一个括号。
(show-paren-mode t)
(setq show-paren-style 'parentheses)

;;显示行号
;;(global-linum-mode t)

;;显示列号。
(setq column-number-mode t)

;;配置tab
(setq tab-width 4 indent-tabs-mode  nil)

;;改变emacs固执的要你回答yes的行为
(fset 'yes-or-no-p 'y-or-n-p)


(server-start)

(auto-image-file-mode t)

;; 设置标记快捷键"C-@" to C-x <SPC>, C-c <SPC>被org-mode中的 org-table-blank-field绑定
(global-set-key (kbd "C-x <SPC>") 'set-mark-command)

;; 设置当没有标记的时候，复制当前行
(defun my-kill-ring-save (&optional n)
  "if region is active, copy region.Otherwise, copy line."
  (interactive "p")
  (if mark-active
	(progn (copy-region-as-kill (region-beginning) (region-end))
	(message "Copy region"))
     (if (> n 0)
		(let ((beg (line-beginning-position))
		  (end (line-end-position n)))
		(copy-region-as-kill beg end))
		(message "Copy line"))))
(global-set-key (kbd "M-w") 'my-kill-ring-save)

;; 复制一个单词
(defun my-copy-word (&optional arg)
 "Copy word at point"
 (interactive "P")
 (let ((beg (progn (if (looking-back "[^\s^\n^\r^\.]" 1) (backward-word 1)) (point)))
	(end (progn (forward-word arg) (point))))
	(copy-region-as-kill beg end)
	(message "Copy word begin:%d,end:%d" beg end)))
(global-set-key (kbd "C-c w") 'my-copy-word)

(defun my-kill-word (&optional arg)
 "Kill whole word at point"
 (interactive "P")
 (let ((beg (progn (if (looking-back "[^\s^\n^\r]" 1) (backward-word 1)) (point)))
	(end (progn (forward-word arg) (point))))
	(kill-region beg end)
	(message "kill word begin:%d,end:%d" beg end)))
(global-set-key (kbd "C-c k") 'my-kill-word)

;; 复制一个段落
(defun my-copy-paragraph (&optional arg)
 "Copy paragraphes at point"
 (interactive "P")
 (let ((beg (progn (backward-paragraph 1) (point)))
   (end (progn (forward-paragraph arg) (point))))
 (copy-region-as-kill beg end)
 (message "Copy paragraph"))
)
(global-set-key (kbd "C-c C-p") 'my-copy-paragraph)

(defun fullscreen ()
  (interactive)
    (set-frame-parameter nil 'fullscreen
      (if (frame-parameter nil 'fullscreen) nil 'fullboth)))
(global-set-key [(f11)] 'fullscreen)

;;所有的备份文件都放置在~/backups目录下
(setq backup-directory-alist (quote (("." . "~/backups"))))
(setq version-control t)
(setq kept-old-versions 2)
(setq kept-new-versions 5)
(setq delete-old-versions t)
(setq backup-by-copying t)
;;emacs中，改变文件时，默认都会产生备份文件（以~结尾的文件）。可以完全去掉
;;（并不可取），也可以制定备份的方式。这里采用的是，把所有的文件备份都放在一个
;;固定的地方。对于每个备份文件，保留最原始的两个版本和最新的五个版本。
;;并且备份的时候，备份文件是复件，而不是原件。

(prefer-coding-system 'utf-8)
(defun current-line-empty-p ()
  (save-excursion
    (beginning-of-line)
    (looking-at "[[:space:]]*$")))

(defun toggle-comment-on-line ()
    "Comments or uncomments the region or the current line if there's no active region."
    (interactive)
	(if (current-line-empty-p) (comment-indent)
    (let (beg end)
        (if (region-active-p)
            (setq beg (region-beginning) end (region-end))
            (setq beg (line-beginning-position) end (line-end-position)))
        (comment-or-uncomment-region beg end))))

(global-set-key (kbd "C-/") 'toggle-comment-on-line)

(defun kill-other-all-buffers ()
  "Kill all other buffers."
  (interactive)
  (mapc 'kill-buffer (delq (current-buffer) (buffer-list))))

(defun kill-all-buffers ()
(interactive)
   (mapc 'kill-buffer (buffer-list)))

(defun kill-other-buffer2 ()
  "kill other buffer exclude current and  start with '*'"
  (interactive)
  (mapc
   (lambda (x)
     (let ((name (buffer-name x)))
       (unless (or (string-prefix-p "*" name)
		   (string-prefix-p " *" name)
		   (eql x (current-buffer)))
	 (message "kill buffer %s %s" name (current-buffer))
	 (kill-buffer x))))
   (buffer-list)))

(defun kill-other-buffer ()
  "kill other buffer exclude current and  start with '*'"
  (interactive)
  (mapc
   (lambda (x)
     (let ((name (buffer-name x)))
       (if (and (buffer-file-name x)
                (not (eql x (current-buffer))))
           (kill-buffer x)
         )
       )
     )
   (buffer-list)))

(defun reload-config ()
  (interactive)
  (load-file "~/.emacs.d/init.el")
  )

(defun my-newline()
  "newline of super"
  (interactive)
  (move-end-of-line 1)
  (newline)
  (indent-for-tab-command)
  )

(global-set-key (kbd "C-o") 'my-newline)

(defun quick-copy-line ()
      "Copy the whole line that point is on and move to the beginning of the next line.
    Consecutive calls to this command append each line to the
    kill-ring."
      (interactive)
      (let ((beg (line-beginning-position 1))
            (end (line-beginning-position 2)))
        (if (eq last-command 'quick-copy-line)
            (kill-append (buffer-substring beg end) (< end beg))
          (kill-new (buffer-substring beg end))))
      (beginning-of-line 2))

;; duplicate current line
(defun duplicate-current-line (&optional n)
  "duplicate current line, make more than 1 copy given a numeric argument"
  (interactive "p")
  (save-excursion
    (let ((nb (or n 1))
    	  (current-line (thing-at-point 'line)))
      ;; when on last line, insert a newline first
      (when (or (= 1 (forward-line 1)) (eq (point) (point-max)))
    	(insert "\n"))

      ;; now insert as many time as requested
      (while (> n 0)
    	(insert current-line)
    	(decf n)))))

(global-set-key (kbd "<C-return>") 'duplicate-current-line)

;;(require 'auto-save)            ;; 加载自动保存模块

;; (auto-save-enable)              ;; 开启自动保存功能
;; (setq auto-save-slient t)       ;; 自动保存的时候静悄悄的， 不要打扰我

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(setq dired-recursive-copies (quote always)) ; “always” means no asking
(setq dired-recursive-deletes (quote top)) ; “top” means ask once

;;set windows numbering
(use-package window-numbering
  :init
  (window-numbering-mode 1)
  (setq dired-dwim-target t)
  (setq x-wait-for-event-timeout nil))


(use-package popwin
  :init
  (require 'popwin)
  (popwin-mode 1)
)


(use-package powerline
  :init
      (powerline-default-theme)
)
(provide 'init-common)
