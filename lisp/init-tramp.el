(use-package tramp
  :config
  (when (eq system-type 'windows-nt)
    (setq default-tramp-method "plink")
    )
  (when (eq system-type 'gnu/linux)
    (setq default-tramp-method "ssh")
    )
  (when (eq system-type 'darwin)
    (setq default-tramp-method "ssh")
    )
  )
(provide 'init-tramp)
