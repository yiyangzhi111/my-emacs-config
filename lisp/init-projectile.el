(use-package projectile
  :init
  (projectile-mode t)
  ;; (projectile-register-project-type 'go '("go.mod")
                                    ;; :project-file "go.mod"
                                    ;; :compile "go build"
                                    ;; :test "go test"
  ;;                                  )
  :bind(("M-s a" . projectile-ack)
        ("M-s s" . projectile-ag))
  :config
  ;; (add-to-list 'projectile-project-root-files-bottom-up "pubspec.yaml")
  ;; (add-to-list 'projectile-project-root-files-bottom-up "BUILD")
  (setq projectile-globally-ignored-file-suffixes '(".exe", ".run"))

  )
(provide 'init-projectile)
