(use-package lua-mode
  :config
  (setq lua-indent-level 2)
  )
(provide 'init-lua)
