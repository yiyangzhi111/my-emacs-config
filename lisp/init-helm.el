;; display helm window at bottom
;; https://github.com/syl20bnr/spacemacs/blob/master/layers/+spacemacs/spacemacs-completion/funcs.el                                        ;
(defvar spacemacs-helm-display-help-buffer-regexp '("\\*.*Helm.*Help.*\\*"))
(defvar spacemacs-helm-display-buffer-regexp `("\\*.*helm.*\\*"
                                               (display-buffer-in-side-window)
                                               (inhibit-same-window . nil)
                                               (side . bottom)
                                               (window-width . 0.6)
                                               (window-height . 0.4)))

(defun display-helm-at-bottom (buffer &optional _resume)
  (let ((display-buffer-alist (list spacemacs-helm-display-help-buffer-regexp
                                    spacemacs-helm-display-buffer-regexp)))
    ;; (display-buffer buffer)
    (helm-default-display-buffer buffer)
    ))

(use-package helm
;;  :straight t
  :config
;;    (require 'helm-config)
;;    (helm-mode 1)
    (setq helm-display-function 'display-helm-at-bottom)

    (global-set-key (kbd "M-x")                        'helm-M-x)
    (global-set-key (kbd "C-x C-b") 'helm-mini)
    (global-set-key (kbd "C-x r b")                      'helm-filtered-bookmarks)
    (global-set-key (kbd "C-x C-f")                    'helm-find-files)
    (global-set-key (kbd "C-.") 'helm-imenu-anywhere)
    ;; (setq helm-projectile-fuzzy-match nil)
    ;; (require 'helm-projectile)
    ;; (helm-autoresize-mode 1)
  :bind(("M-s g" . helm-projectile-rg))

  )

(use-package imenu-anywhere)

(use-package helm-projectile
  :init
    (helm-projectile-on)
)

(use-package helm-descbinds
  :init
    (helm-descbinds-mode)
    (global-set-key (kbd "C-h b")                    'helm-descbinds)
)


(provide 'init-helm)
