(add-to-list 'auto-mode-alist '("CMakeLists\\.txt\\'" . cmake-mode))
(add-to-list 'auto-mode-alist '("\\.cmake\\'" . cmake-mode))

(use-package cmake-ide
  :init
  (require 'rtags) ;; optional, must have rtags installed
  (cmake-ide-setup)
  :bind (("C-c c" . cmake-ide-compile))
)
