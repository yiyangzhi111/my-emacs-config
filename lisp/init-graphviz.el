(defun compilation-exit-autoclose (status code msg)
  ;; If M-x compile exists with a 0
  (when (and (eq status 'exit) (zerop code))
    ;; then bury the *compilation* buffer, so that C-x b doesn't go there
    (bury-buffer)
    ;; and delete the *compilation* window
    (delete-window (get-buffer-window (get-buffer "*compilation*"))))
  ;; Always return the anticipated result of compilation-exit-message-function
  (cons msg code))
(use-package graphviz-dot-mode
  :init
  ;; Helper for compilation. Close the compilation window if
  ;; there was no error at all.
  ;; Specify my function (maybe I should have done a lambda function)
  :config
  (setq compilation-exit-message-function 'compilation-exit-autoclose)
  (setq compilation-read-command nil)
  (setq graphviz-dot-mode-map
        (let ((m (make-sparse-keymap)))
          (define-key m (kbd "C-c C-p") 'graphviz-dot-preview)
          (define-key m (kbd "C-c C-c") 'compile)
          m))
  )
(provide 'init-graphviz)

