(use-package web-mode
  :init
  (setq auto-mode-alist
      (append
       '(("\\.js\\'" . js2-mode))
       '(("\\.html\\'" . web-mode))
       auto-mode-alist))
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq-default indent-tabs-mode nil)
)
(use-package js2-mode
  :init
  (setq js2-strict-missing-semi-warning nil)
  )
(provide 'init-web)
