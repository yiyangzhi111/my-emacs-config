(defun debug-enter()
  "enter debug mode."
  (interactive)
  (dap-mode 1)
  (dap-ui-mode 1)
  ;; enables mouse hover support
  (dap-tooltip-mode 1)
  ;; use tooltips for mouse hover
  ;; if it is not enabled `dap-mode' will use the minibuffer.
  (tooltip-mode 1)
  (require 'dap-go)
  (message "start debug model")
  )
(defun debug-exit()
  "exit debug mode"
  (interactive)
  (dap-mode 0)
  (dap-ui-mode 0)
  ;; enables mouse hover support
  (dap-tooltip-mode 0)
  ;; use tooltips for mouse hover
  ;; if it is not enabled `dap-mode' will use the minibuffer.
  (tooltip-mode 0)
  (require 'dap-go)
  (message "stop debug model")
)
(use-package dap-mode
  :init
  :bind (("C-c d" . debug-enter)
         ("C-c e" . debug-exit))
)
(provide 'init-dap)
