(use-package eshell
  :init
  (setq eshell-aliases-file (concat user-emacs-directory "/plugin/eshell/aliases"))
  (defalias 'open 'find-file)
  (defalias 'openo 'find-file-other-window)
  (add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)
  )

;; (add-to-list 'load-path (expand-file-name "~/elisp/aweshell"))
;;(require 'aweshell)

(provide 'init-eshell)
