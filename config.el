;; this function copy from: http://www.emacswiki.org/emacs/DotEmacsModular
(defun recursive-subdirs (directory &optional withroot)
  "Return a unsorted list of names of directories in DIRECTORY recursively.
If WITHROOT is non-nil, also DIRECTORY will be include."
  (let (subdirs)
    (dolist (element (directory-files-and-attributes directory nil nil nil))
      (let* ((path (car element))
             (isdir (car (cdr element)))
             (ignore (or (string= path ".") (string= path ".."))))
        (if (and (eq isdir t) (not ignore))
            (let ((dir (concat directory "/" path)))
              (setq subdirs (append (cons dir subdirs)
                                    (recursive-subdirs dir)))))))
    (if (not (eq withroot nil))
        (add-to-list 'subdirs directory))
    subdirs))

;; add all subdirs of directory to load-path ::super
(defun add-dirs-to-list (directory &optional withroot)
  (dolist (element (recursive-subdirs directory withroot))
    (add-to-list 'load-path element)
    )
  )
;;(let ((installpath (concat superdir "/config/elpa.el")))
;;      (load-file installpath)
;;)
;; must define superdir in you .emacs file ::super
                                        ;'(defvar superdir "./")
;; use-package to make config more simple
(load (concat superdir "/elpa.el"))
(with-eval-after-load "use-package"
  (eval-when-compile
    (require 'use-package))
  (require 'diminish)                ;; if you use :diminish
  (require 'bind-key)                ;; if you use any :bind variant

  (use-package benchmark-init
  :ensure t
  :config
  ;; To disable collection of benchmark data after init is done.
  (add-hook 'after-init-hook 'benchmark-init/deactivate))
  (let ((configpath (concat superdir "/config"))
	(pluginpath (concat superdir "/plugin")))
    (add-dirs-to-list pluginpath t)
    (mapc 'load (directory-files configpath t "^[a-zA-Z0-9\-].*.el$"))
    )
  )

(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."

                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))


;;(let ((orgpath (concat superdir "/org/init.el")))
;;	(load orgpath)
;;)
