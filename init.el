;; make emacs start faster
;; https://github.com/hlissner/doom-emacs/blob/develop/docs/faq.org
(setq gc-cons-threshold most-positive-fixnum ; 2^61 bytes
      gc-cons-percentage 0.6)
(add-hook 'emacs-startup-hook
  (lambda ()
    (setq gc-cons-threshold 16777216 ; 16mb
          gc-cons-percentage 0.1)))

;;(defun doom-defer-garbage-collection-h ()
;;  (setq gc-cons-threshold most-positive-fixnum))

;;(defun doom-restore-garbage-collection-h ()
  ;; Defer it so that commands launched immediately after will enjoy the
  ;; benefits.
;;  (run-at-time
;;   1 nil (lambda () (setq gc-cons-threshold doom-gc-cons-threshold))))

;;(add-hook 'minibuffer-setup-hook #'doom-defer-garbage-collection-h)
;;(add-hook 'minibuffer-exit-hook #'doom-restore-garbage-collection-h)

;; refer: https://github.com/seagle0128/.emacs.d
(defun update-load-path (&rest _)
  "Update `load-path'."
  (dolist (dir '("site-lisp" "lisp"))
    (push (expand-file-name dir user-emacs-directory) load-path)))
;;
(defun add-subdirs-to-load-path (&rest _)
  "Add subdirectories to `load-path'."
  (let ((default-directory (expand-file-name "site-lisp" user-emacs-directory)))
    (normal-top-level-add-subdirs-to-load-path)))

(advice-add #'package-initialize :after #'update-load-path)
(advice-add #'package-initialize :after #'add-subdirs-to-load-path)
;;
(update-load-path)
(add-subdirs-to-load-path)

(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."

                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))


;; Packages
;; Without this comment Emacs25 adds (package-initialize) here
(require 'init-elpa)

;; Preferences
(require 'init-common)
;;(require 'init-theme)
(require 'init-doom-themes)
(require 'init-helm)
;;(require 'init-ivy)
(require 'init-env)
(require 'init-org)
(require 'init-yasnippet)
(require 'init-acejump)
(require 'init-golang)
(require 'init-python)
(require 'init-lsp)
(require 'init-eshell)
;; (require 'init-coin)
(require 'init-flycheck)
(require 'init-git)
(require 'init-graphviz)
(require 'init-image)
;;(require 'init-jump)
;;(require 'init-lua)
(require 'init-md)
(require 'init-projectile)
;; (require 'init-recent)
(require 'init-tramp)
(require 'init-vue)
(require 'init-web)
(require 'init-dap)
(require 'init-cc)
(require 'init-org-protocol)
;;(require 'swig-mode)
;;(require 'aweshell)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("e1f4f0158cd5a01a9d96f1f7cdcca8d6724d7d33267623cc433fe1c196848554" "2721b06afaf1769ef63f942bf3e977f208f517b187f2526f0e57c1bd4a000350" "2853dd90f0d49439ebd582a8cbb82b9b3c2a02593483341b257f88add195ad76" "683b3fe1689da78a4e64d3ddfce90f2c19eb2d8ab1bab1738a63d8263119c3f4" "a589c43f8dd8761075a2d6b8d069fc985660e731ae26f6eddef7068fece8a414" "016f665c0dd5f76f8404124482a0b13a573d17e92ff4eb36a66b409f4d1da410" "a44e2d1636a0114c5e407a748841f6723ed442dc3a0ed086542dc71b92a87aee" "a138ec18a6b926ea9d66e61aac28f5ce99739cf38566876dc31e29ec8757f6e2" "7a424478cb77a96af2c0f50cfb4e2a88647b3ccca225f8c650ed45b7f50d9525" "be84a2e5c70f991051d4aaf0f049fa11c172e5d784727e0b525565bb1533ec78" "467dc6fdebcf92f4d3e2a2016145ba15841987c71fbe675dcfe34ac47ffb9195" "4ff1c4d05adad3de88da16bd2e857f8374f26f9063b2d77d38d14686e3868d8d" "66bdbe1c7016edfa0db7efd03bb09f9ded573ed392722fb099f6ac6c6aefce32" default))
 '(display-time-format "%H:%M")
 '(lsp-go-directory-filters ["-**/node_modules"])
 '(magit-todos-insert-after '(bottom) nil nil "Changed by setter of obsolete option `magit-todos-insert-at'")
 '(package-selected-packages
   '(auto-virtualenv org-protocol-jekyll d2-mode just-mode lsp-dart dart-mode typescript-mode rust-mode org-download yaml-mode protobuf-mode tmux-pane imenu-anywhere helm-rg yasnippet-snippets treemacs-all-the-icons treemacs-projectile all-the-icons window-numbering web-mode vue-mode powerline popwin magit-todos lsp-ui js2-mode image+ helm-projectile helm-lsp helm-descbinds graphviz-dot-mode go-mode gnu-elpa-keyring-update flycheck elpy doom-themes diminish diff-hl dap-mode ace-jump-mode)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
