#My Emacs config ::super
Install:
## golang
You may need to do this, when you use golang mode:
need:
1、install tools:

``` shell
go install honnef.co/go/tools/cmd/staticcheck@latest
go install github.com/derekparker/delve/cmd/dlv@latest
go install golang.org/x/tools/cmd/goimports@latest
```

## python depends

	pip install jedi flake8 importmagic autopep8 rope yapf

Use elpy in python mode.

You can use 'C-c C-v' to run flake code lint

## c++
```
sudo apt-get install clangd bear
```

then run bear in your project:

```
bear make
```


## other

if you want to use epub mode,you should to get html2text from:

http://www.mbayer.de/html2text

in windows,can use this file to get html2text

http://www.opencats.org/downloads/setupResumeIndexingTools.exe

epub source:

http://sourceforge.net/projects/epubmode/

need to debug in windows
